#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import csv

if __package__ is None:
    import sys
    _path = os.path.abspath(os.path.join(__file__, '../..'))
    if not _path in sys.path:
        sys.path.append(_path)

from log import log


class ReadCSV(object):
    """docstring for ReadCSV"""

    def __init__(self, csv_file):
        super(ReadCSV, self).__init__()
        self.csv_file = csv_file

    def get_csv_rows(self, csv_file):
        # csv_rows = []
        with open(csv_file, 'r') as file_obj:
            reader = csv.DictReader(file_obj, delimiter=',')
            for row in reader:
                yield row

    @property
    def row_num(self):
        return len(self.csv_rows)

    @property
    def row_gen(self):
        return(self.get_csv_rows(self.csv_file))

    @property
    def csv_rows(self):
        return(list(self.get_csv_rows(self.csv_file)))

    def read_row(self, row_idx):
        return(self.csv_rows[row_idx])


class _PersonalData(object):
    """Wrapper around concept of a "personal data".
    name country phone state city address
    """
    #--------------------------------------------------------------------------
    # object
    #--------------------------------------------------------------------------

    def __init__(self, name=None, phone=None, country=None, state=None, city=None, address=None, full_address=None):
        self._name = name
        self._country = country
        self._phone = phone
        self._state = state
        self._city = city
        self._address = address

        self._full_address = full_address
        self._full_address = self.full_address

    def __str__(self):
        return self._name

    def __repr__(self):
        return self._name

    def __eq__(self, other):
        return False if (other is None) else (self.__class__ == other.__class__)

    def __ne__(self, other):
        return not (self == other)

    #--------------------------------------------------------------------------
    # accessors
    # name country phone state city address
    #--------------------------------------------------------------------------

    def get(self, source):
        return (getattr(self, source))
        # return dataSource

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def country(self):
        return self._country

    @country.setter
    def country(self, value):
        self._country = value

    @property
    def phone(self):
        return self._phone

    @phone.setter
    def phone(self, value):
        self._phone = value

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value

    @property
    def city(self):
        return self._city

    @city.setter
    def city(self, value):
        self._city = value

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, value):
        self._address = value

    @staticmethod
    def add_comma(*str_args):
        return ','.join(str_args)

    @property
    def full_address(self):
        if not None in (self._address, self._city, self._state, self._country):
            self._full_address = (self.add_comma(
                self._address, self._city, self._state, self._country))
        return self._full_address

    @full_address.setter
    def full_address(self, value):
        self._full_address = value

    #--------------------------------------------------------------------------


class ContactList(object):
    """docstring for ContactList"""

    def __init__(self, csv_file):
        super(ContactList, self).__init__()
        self.csv_file = csv_file
        self.row_table = None
        # self.load_data()

    def item_gen(self):
        csv_inst = ReadCSV(self.csv_file)
        for c_dict in csv_inst.csv_rows:
            c_dict = dict((k.lower(), v) for k, v in c_dict.iteritems())
            _per = _PersonalData(**c_dict)
            yield _per

    def load_table(self):
        self.row_table = (list(self.item_gen()))
        log.info('CALL ContactList.load_table(void) -> %s' % self.row_table)
        return(self.row_table)

    @property
    def row_num(self):
        return len(self.row_table)

    def delete_item(self, idx):
        self.row_table.pop(idx)

    def insert_item(self, idx, name, phone, address):
        if self.row_table == None:
            self.load_table()

        if self.row_num <= idx:
            log.error(("Row index is out of range %s! -> " % self.row_num) + idx)

        _per = _PersonalData(name=name, phone=phone, full_address=address)
        self.row_table.insert(idx, _per)


if __name__ == '__main__':
    if __package__ is None:
        import sys
        _path = os.path.abspath(os.path.join(__file__, '../..'))
        if not _path in sys.path:
            sys.path.append(_path)
        from log import log
        from controller import utility

    XLS_FILE = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.xls'))
    CSV_FILE = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.csv'))
    PICKLE_FILE = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.pkl'))
    JSON_FILE = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.json'))
    YAML_FILE = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.yaml'))

    inst = ReadCSV(CSV_FILE)
    # print list(inst.csv_rows)
    # for c in inst.csv_rows:
    #     pass

    inst = ContactList(CSV_FILE)
    table = inst.load_table()

    utility.write_json(table, JSON_FILE)
    utility.write_yaml(table, YAML_FILE)
    print table
