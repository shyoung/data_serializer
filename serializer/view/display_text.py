#!/usr/bin/python
# -*- coding: utf-8 -*-

import os


class TextFormat (object):
    """
    a TableCell object is used to create a cell in a HTML table. (td or th)
    """

    def __init__(self, text_length=None, header=False):
        self.header = header
        self.text_length = text_length

    def __str__(self):
        if self.header:
            return ("{:=^%s}" % self.text_length)
        else:
            return ("{:>%s}" % self.text_length)


class TextTable(object):
    """docstring for TextTable"""

    def __init__(self, row_matrix=None, title_row=[]):
        super(TextTable, self).__init__()

        self.row_matrix = row_matrix
        self.col_matrix = zip(*row_matrix)

        self.cell_length = 15
        self.title_row = title_row

    @staticmethod
    def item_length_list(data_list):
        """
        data_list = ["5", "abc", "How are you?"]
        rtn = item_length_list(data_list)
        print rtn
        >>>  [1, 3, 12]
        """
        data_list = map(str, data_list)
        data_list = map(len, data_list)
        return data_list

    # get max length in each column
    def col_width_list(self):
        """
        By counting the charactor numbers in each cell, get the maximum width for each column.
        Here's the column width list for the following example:
        >> [15, 15, 21]
        -------------------------------------------------------
        |====Man Utd====|===Man City====|====T Hotspurddd=====|
        |              1|              2|                    1|
        |              0|   How are you?|                    0|
        |             Hi|              4|  What date is today?|
        -------------------------------------------------------
        """

        # get max length from column matrix in each column
        data_length_list = [(self.item_length_list(col_data)) for col_data in self.col_matrix]
        print data_length_list
        data_length_list = [max(self.item_length_list(col_data)) for col_data in self.col_matrix]

        # get max length from title list in each column
        title_length_list = self.item_length_list(self.title_row)

        # Intersection of above lists
        max_length_list = map(max, data_length_list, title_length_list)

        # set minimum value into list
        max_length_list = [i + 2 if i >
                           self.cell_length else self.cell_length for i in max_length_list]

        return max_length_list

    @staticmethod
    def table_row_format(max_length_list):
        """
        Create title row format and content row format.
        input  >>>  [15, 15, 21]
        output >>>  ('|{:=^15}|{:=^15}|{:=^21}|\n', '|{:>15}|{:>15}|{:>21}|\n')
        """
        print max_length_list
        title_format = ''
        row_format = ''

        for l in max_length_list:
            title_format += ('|' + str(TextFormat(l, header=True)))
            row_format += ('|' + str(TextFormat(l)))
        title_format += '|\n'
        row_format += '|\n'
        print(title_format, row_format)
        return(title_format, row_format)

    def text_table(self):
        """
        Compose each row data as a formated text table
        """
        # get title format and content row format
        title_format, row_format = self.table_row_format(self.col_width_list())

        print self.title_row
        title_str = title_format.format(*self.title_row)
        print title_str
        total_length = len(title_str) - 1

        rtn_str = ''
        rtn_str += '-' * total_length + '\n'
        rtn_str += title_str
        for row in self.row_matrix:
            rtn_str += row_format.format(*row)
        rtn_str += '-' * total_length
        return rtn_str

    def __str__(self):
        return (self.text_table())


def table(*args, **kwargs):
    rtn_str = str(TextTable(*args, **kwargs))
    return rtn_str


if __name__ == '__main__':

    title_row = ["Man Utd", "Man City", "T Hotspurddd"]
    row_matrix = [[1, 2, 1],
                  [0, 'How are you?', 0],
                  ['Hi', 4, 'What date is today?']]
    # -----------------------------------------
    table_inst = TextTable(row_matrix, title_row=title_row)
    print str(table_inst)
