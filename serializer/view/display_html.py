#!/usr/bin/python
# -*- coding: utf-8 -*-

TABLE_STYLE = """
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
"""


class TableCell (object):
    """
    a TableCell object is used to create a cell in a HTML table. (td or th)
    """

    def __init__(self, text="null", header=False):
        self.text = text
        self.header = header

    def __str__(self):
        """return the HTML code for the table cell as a string"""
        # if self.text:
        #     text = str(self.text)

        if self.header:
            return '  <th>%s</th>\n' % (self.header)
        else:
            return '  <td>%s</td>\n' % (self.text)


class TableRow (object):
    """
    create <tr> tag for HTML table
    """

    def __init__(self, cells=None, header=False):

        self.cells = cells
        self.header = header

    def __str__(self):
        """return the HTML code for the table row as a string"""

        result = ' <tr>\n'
        for cell in self.cells:
            col = self.cells.index(cell)    # cell column index
            if not isinstance(cell, TableCell):
                cell = TableCell(cell, header=self.header)
            result += str(cell)
        result += ' </tr>\n'
        return result


class Table (object):
    """
    create <table> tag for HTML table
    """

    def __init__(self, rows=None, header_row=None):

        self.header_row = header_row
        self.rows = rows
        if not rows:
            self.rows = []

    def __str__(self):
        """return the HTML code for the table as a string"""

        result = '<table>\n'

        # insert a header row
        if self.header_row:
            if not isinstance(self.header_row, TableRow):
                result += str(TableRow(self.header_row, header=True))
            else:
                result += str(self.header_row)

        # data rows
        for row in self.rows:
            if not isinstance(row, TableRow):
                row = TableRow(row)
            result += str(row)
        result += '</table>'
        return result


def table(*args, **kwargs):
    'table HTML code as a string.'
    # print kwargs
    rtn_str = ''
    if 'style' in kwargs:
        del kwargs['style']
        rtn_str += TABLE_STYLE
    rtn_str += str(Table(*args, **kwargs))
    return rtn_str


if __name__ == '__main__':

    t = Table()
    t.rows.append(TableRow(['A', 'B', 'C'], header=True))
    t.rows.append(TableRow(['D', 'E', 'F']))
    t.rows.append(('i', 'j', 'k'))
    # f.write(str(t) + '<p>\n')
    r = TableRow(['A', 'B', 'C'], header=True)
    print str(r)

    r = TableRow(['A', 'B', 'C'])
    print str(r)
