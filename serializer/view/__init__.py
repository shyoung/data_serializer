#!/usr/bin/env python
# -*- coding: utf-8 -*-

import display


def create_html(controller, html_file):
    return display.display_html_table(controller, html_file)


def create_txt(controller, txt_file):
    return display.display_text_table(controller, txt_file)
