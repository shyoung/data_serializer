#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import display_html
import display_text


def write_file(data, path):
    """Write data to path as file
    """
    preset_dir = os.path.dirname(path)
    if not os.path.isdir(preset_dir):
        os.makedirs(preset_dir, 0777)
    with open(path, 'w') as file_obj:
        file_obj.write(data)
    os.chmod(path, 0777)
    return path


def display_html_table(ctrl, html_file):
    # generate HTML code
    html_code = display_html.table(ctrl.request_table, ctrl.request_title, style=True)

    # create HTML file
    write_file(html_code, html_file)
    return html_code


def display_text_table(ctrl, txt_file):
    # display as text table
    table_str = display_text.table(list(ctrl.request_table), ctrl.request_title)

    # create text file
    write_file(table_str, txt_file)
    return table_str


if __name__ == '__main__':
    if __package__ is None:
        import sys
        sys.path.append(os.path.abspath(os.path.join(__file__, '../..')))
        from log import log
        from controller import controller
    else:
        from ..log import log

    # input
    csv_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.csv'))

    yaml_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.yaml'))
    json_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.json'))
    pkl_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.pkl'))

    # output
    html_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.html'))
    txt_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.txt'))

    print controller.TITLE_LIST
    controller = controller.Impl(csv_file)
    controller.load_table()

    html = display_html_table(controller, html_file)
    # log.debug('html: ')
    # log.debug(html)
    txt = display_text_table(controller, txt_file)
    # log.debug('txt: ')
    # log.debug(txt)
