#!/usr/bin/env python
# -*- coding: utf-8 -*-

import controller


def supported_formats():
    return controller.SUPPORTED_FORMAT


def create(csv_file=None):
    inst = controller.Impl(csv_file)
    if csv_file != None:
        inst.load_table()
    return inst
