#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import sys
import pickle
import json

import sys
_path_list = [os.path.normpath(os.path.join(__file__, '../..')),
              os.path.normpath(os.path.join(__file__, '../../../py_module'))]
for p in _path_list:
    if not p in sys.path:
        sys.path.append(p)

import yaml
from model.contact import _PersonalData


class OrderedDictYAMLLoader(yaml.Loader):

    """
    A YAML loader that loads mappings into ordered dictionaries.

    Ref: https://gist.github.com/enaeseth/844388
    """

    def __init__(self, *args, **kwargs):
        yaml.Loader.__init__(self, *args, **kwargs)

        self.add_constructor(
            u'tag:yaml.org,2002:map', type(self).construct_yaml_map)
        self.add_constructor(
            u'tag:yaml.org,2002:omap', type(self).construct_yaml_map)

    def construct_yaml_map(self, node):
        data = nextcollections.OrderedDict()
        yield data
        value = self.construct_mapping(node)
        data.update(value)

    def construct_mapping(self, node, deep=False):
        if isinstance(node, yaml.MappingNode):
            self.flatten_mapping(node)
        else:
            raise yaml.constructor.ConstructorError(None, None,
                                                    'expected a mapping node, but found %s' % node.id, node.start_mark)

        mapping = nextcollections.OrderedDict()
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            try:
                hash(key)
            except TypeError, exc:
                raise yaml.constructor.ConstructorError(
                    'while constructing a mapping',
                    node.start_mark, 'found unacceptable key (%s)' % exc, key_node.start_mark)
            value = self.construct_object(value_node, deep=deep)
            mapping[key] = value
        return mapping


class CustomEncoder(json.JSONEncoder):
    '''
    Load class object as dictionary
    REF: https://code.tutsplus.com/tutorials/serialization-and-deserialization-of-python-objects-part-1--cms-26183
    '''

    def default(self, o):
        return {'__{}__'.format(o.__class__.__name__): o.__dict__}


def decode_object(o):
    if '___PersonalData__' in o:
        p = _PersonalData()
        p.__dict__.update(o['___PersonalData__'])
        return p
    return o


def read_json(path):
    """Read data from path as json file
    """
    with open(path) as file_obj:
        data = file_obj.read()
        return json.loads(data, object_hook=decode_object)


def read_pickle(path):
    with open(path) as f:
        return pickle.load(f)


def read_yaml(path, keep_order=False):
    """Read data from path as yaml file
    """
    with open(path) as file_obj:
        if keep_order is True:
            return yaml.load(file_obj, OrderedDictYAMLLoader)
        else:
            return yaml.load(file_obj)


def write_json(data, path):
    """Write data to path as json file
    """
    preset_dir = os.path.dirname(path)
    if not os.path.isdir(preset_dir):
        os.makedirs(preset_dir, 0777)
    with open(path, 'w') as file_obj:
        file_obj.write(
            json.dumps(data, sort_keys=True, indent=4, cls=CustomEncoder, separators=(',', ': ')))
    os.chmod(path, 0777)


def write_yaml(data, path):
    """Write data to path as yaml file
    """
    preset_dir = os.path.dirname(path)
    if not os.path.isdir(preset_dir):
        os.makedirs(preset_dir, 0777)
    with open(path, 'w') as file_obj:
        yaml.dump(data, file_obj, default_flow_style=False)
    os.chmod(path, 0777)


def write_pickle(data, path):
    with open(path, 'w') as f:
        pickle.dump(data, f)
    os.chmod(path, 0777)


def write_file(data, path):
    """Write data to path as file
    """
    preset_dir = os.path.dirname(path)
    if not os.path.isdir(preset_dir):
        os.makedirs(preset_dir, 0777)
    with open(path, 'w') as file_obj:
        file_obj.write(data)
    os.chmod(path, 0777)


if __name__ == '__main__':
    pass
