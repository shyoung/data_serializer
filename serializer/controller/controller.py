#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from __init__ import USE_CLUSTER
import os

if __package__ is None:
    import sys
    _path = os.path.abspath(os.path.join(__file__, '../..'))
    if not _path in sys.path:
        sys.path.append(_path)

import utility
from log import log


# ORDER_LIST = ['name', 'country', 'phone', 'state', 'city', 'address']
ORDER_LIST = ['name', 'phone', 'full_address']
TITLE_LIST = ['Name', 'Phone', 'Address']
SUPPORTED_FORMAT = ['.yaml', '.json', '.pkl']


class Impl(object):

    title_list = TITLE_LIST
    filetype_list = SUPPORTED_FORMAT

    def __init__(self, csv_file=None):

        super(Impl, self).__init__()
        global log
        self.csv_file = csv_file
        self.row_table = None
        self._metadata_file = None

        self.itemObj = self.__create_core__()

    #==============================================

    def __create_core__(self):
        try:
            from model.contact import ContactList
            # from scripts.core.get_item_info import itemInfoObj
            log.debug('CALL __create_core__(void) -> %s' % ContactList)
            return ContactList(self.csv_file)
        except Exception as e:
            log.error('CALL __create_core__(void) -> failed: %s' % e)
            return None

    def load_table(self):
        if self.file_exists(self.csv_file):
            self.row_table = self.itemObj.load_table()
            return(self.row_table)
        return None

    @property
    def request_title(self):
        return(self.title_list)

    @property
    def request_table(self, order_list=ORDER_LIST):
        if not self.has_value:
            raise StopIteration(None)
        row_item = self.row_table
        count = 0
        for per in row_item:
            count += 1
            yield [per.get(i) for i in order_list]
        log.debug('size of feedback -> %d' % count)

    #==============================================
    @staticmethod
    def get_extension(filename):
        basename = os.path.basename(filename)
        ext = '.'.join(basename.split('.')[1:])
        return '.' + ext if ext else None

    # @staticmethod
    def is_valid_filename(self, in_file):
        _bool = True
        if not isinstance(in_file, basestring):
            _bool = False
        # elif not os.path.isfile(in_file):
        #     _bool = False
        elif not (self.get_extension(in_file) in SUPPORTED_FORMAT):
            _bool = False
        if not _bool:
            log.error('CALL is_valid_filename(in_file) -> Not filename: %s' % in_file)
        return _bool

    @staticmethod
    def file_exists(in_file):
        _bool = True
        if not isinstance(in_file, basestring):
            _bool = False
        elif not os.path.isfile(in_file):
            _bool = False
        elif not os.path.exists(in_file):
            _bool = False

        if not _bool:
            log.error('CALL file_exists(in_file) -> not exist: %s' % in_file)
        return _bool

    @property
    def has_value(self):
        _bool = (self.row_table != None)
        if self.row_table == None:
            log.error('CALL has_value(void) -> missing self.row_table: %s' % 'None')
        return _bool

    @property
    def metadata_file(self):
        return self._metadata_file

    @metadata_file.setter
    def metadata_file(self, value):
        if not (self.get_extension(value) in SUPPORTED_FORMAT):
            log.error('Setter metadata_file(value) -> Not supported filetype: %s' %
                      self.get_extension(value))
            return value
        if self._metadata_file != value:
            self._metadata_file = value

    #==============================================

    def read_file(self, read_functor, ext):
        if self.is_valid_filename(self.metadata_file) and self.get_extension(self.metadata_file) != ext:
            log.error('Call read_file(file) -> Not supported filetype: %s' %
                      self.metadata_file)
            return None
        if self.file_exists(self.metadata_file):
            log.debug('CALL read_file(void) -> %s' % self.metadata_file)
            self.row_table = read_functor(self.metadata_file)
            return self.row_table

    def write_file(self, write_functor, ext):
        if self.get_extension(self.metadata_file) != ext:
            log.error('Call write_file(void) -> Not supported filetype: %s' %
                      self.metadata_file)
            return None
        if not self.has_value:
            return None
        log.debug('CALL write_file(void) -> %s' % self.metadata_file)
        write_functor(self.row_table, self.metadata_file)
        return self.metadata_file

    def write_yaml(self):
        return self.write_file(utility.write_yaml, '.yaml')

    def write_json(self):
        return self.write_file(utility.write_json, '.json')

    def write_pickle(self):
        return self.write_file(utility.write_pickle, '.pkl')

    def read_yaml(self):
        return self.read_file(utility.read_yaml, '.yaml')

    def read_json(self):
        return self.read_file(utility.read_json, '.json')

    def read_pickle(self):
        return self.read_file(utility.read_pickle, '.pkl')

    #==============================================


if __name__ == "__main__":
    import os

    # input
    CSV_FILE = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.csv'))

    # output
    log.debug('(os.getcwd()):  %s' % (os.getcwd()))
    yaml_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.yaml'))
    json_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.json'))
    pickle_file = os.path.abspath(os.path.join(__file__, '../../..', 'contact_list.pkl'))

    controller = Impl(CSV_FILE)
    # print controller.is_valid_filename(CSV_FILE)
    controller.load_table()

    log.debug('yaml: ')
    controller.metadata_file = yaml_file
    controller.write_yaml()
    controller.read_yaml()

    log.debug('json: ')
    controller.metadata_file = json_file
    controller.write_json()
    controller.read_json()

    log.debug('pickle: ')
    controller.metadata_file = pickle_file
    controller.write_pickle()
    controller.read_pickle()
