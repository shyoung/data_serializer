#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
log.py -  log function can log to both console and provided log file.
"""

# Standard modules.
import os
import sys
import logging
LOG_FILENAME = os.path.abspath(os.path.join((os.getcwd()), 'logs_file.log'))


def _log(log_filename):
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        filename=log_filename,
                        filemode='a+')
    # Until here logs only to file
    # define a new Handler to log to console as well
    console = logging.StreamHandler()
    # optional, set the logging level
    console.setLevel(logging.DEBUG)
    # set a format which is the same for console use
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)
    return logging.getLogger('')
log = _log(LOG_FILENAME)
