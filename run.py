#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import optparse

from serializer import controller
from serializer import view
from serializer.log import log


def _get_input_args():
    # Build the usage string for our custom error messages.
    script = os.path.basename(sys.argv[0])
    usage = 'usage: {0} [--help] <filepath>'.format(script)
    if len(sys.argv) > 3:
        print usage
        sys.exit(0)

    # Parse args.
    parser = optparse.OptionParser(usage)
    options, args = parser.parse_args()
    return(options, args)


if __name__ == "__main__":

    # get filepath from input argument
    options, args = _get_input_args()
    valid_options = dict((k, v) for k, v in (options.__dict__.iteritems()) if v != None)
    log.info("Input arguments: " + str(options.__dict__) + ", " + str(args))
    if len(args) == 0:
        log.error("Couldn't get any input file path!")
        sys.exit(0)

    # get csv filepath from input argument
    CSV_FILE = args[0]

    # output cache files and display files to current directory
    pwd = (os.getcwd())

    # CSV_FILE = os.path.abspath(os.path.join(pwd, 'contact_list.csv'))
    HTML_FILE = os.path.abspath(os.path.join(pwd, 'contact_list.html'))
    TXT_FILE = os.path.abspath(os.path.join(pwd, 'contact_list.txt'))

    # output
    log.debug('(os.getcwd()):  %s' % (os.getcwd()))
    yaml_file = os.path.abspath(os.path.join(pwd, 'contact_list.yaml'))
    json_file = os.path.abspath(os.path.join(pwd, 'contact_list.json'))
    pickle_file = os.path.abspath(os.path.join(pwd, 'contact_list.pkl'))

    do_metadata = 1
    do_view = 1

    log.debug('supported format: ' + str(controller.supported_formats()))

    # load data from csv file if other formats doesn't exist (yaml, json, pkl)
    ctrl = controller.create(CSV_FILE)
    if do_metadata:
        log.debug('yaml: ')
        ctrl.metadata_file = yaml_file
        ctrl.write_yaml()
        ctrl.read_yaml()

        log.debug('json: ')
        ctrl.metadata_file = json_file
        ctrl.write_json()
        ctrl.read_json()

        log.debug('pickle: ')
        ctrl.metadata_file = pickle_file
        ctrl.write_pickle()
        ctrl.read_pickle()
        # print list(ctrl.request_table)

    if do_view:
        html = view.create_html(ctrl, HTML_FILE)
        print html
        txt = view.create_txt(ctrl, TXT_FILE)
        print txt
